package colourmath

import (
	"math"
)

type XYZ struct {
	X, Y, Z float64
}

// convertToLab converts XYZ to Lab using the ref white
func (from XYZ) convertToLab (ref XYZ) Lab {

	const e = float64(216/24389)
	const k = float64(24389/27)
	//const e = float64(0.008856)
	//const k = float64(903.3)
	fx := float64(0)
	fy := float64(0)
	fz := float64(0)

	xr := from.X / ref.X
	yr := from.Y / ref.Y
	zr := from.Z / ref.Z

	if xr > e {
		fx = math.Pow(xr, 1.0/3.0)
	} else {
		fx = (k * xr + 16) / 116
	}

	if yr > e {
		fy = math.Pow(yr, 1.0/3.0)
	} else {
		fy = (k * yr + 16) / 116
	}

	if zr > e {
		fz = math.Pow(zr, 1.0/3.0)
	} else {
		fz = (k * zr + 16) / 116
	}

	convertL := 116*fy - 16
	convertA := 500 * (fx - fy)
	convertB := 200 * (fy - fz)

	return Lab{convertL, convertA, convertB}
}