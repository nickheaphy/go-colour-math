package colourmath

import (
	"math"
)

type Lab struct {
	L, A, B float64
}

// convertToXYZ takes a lab value and converts it to XYZ using ref as the white reference
func (from Lab) convertToXYZ (ref XYZ) XYZ {
	// http://www.brucelindbloom.com/index.html?Eqn_Lab_to_XYZ.html

	const e = float64(216/24389)
	const k = float64(24389/27)
	//const e = float64(0.008856)
	//const k = float64(903.3)

	fy := (from.L + 16)/116
	fz := fy - (from.B/200)
	fx := (from.A/500) + fy
	xr := float64(0)
	yr := float64(0)
	zr := float64(0)

	if math.Pow(fx,3) > e {
		xr = math.Pow(fx,3)
	} else {
		xr = (116 * fx -16)/k
	}

	if from.L > k*e {
		yr = math.Pow(((from.L + 16)/116),3)
	} else {
		yr = from.L / k
	}

	if math.Pow(fz,3) > e {
		zr = math.Pow(fz,3)
	} else {
		zr = (116 * fz - 16)/k
	}

	convertedX := xr * ref.X
	convertedY := yr * ref.Y
	convertedZ := zr * ref.Z

	return XYZ{convertedX, convertedY, convertedZ}

}