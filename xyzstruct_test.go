package colourmath

import (
	"testing"
	//"fmt"
)

// check the two XYZ colours are 'equal'
func checkLabequal(first Lab, second Lab) bool {
	return floatEquals(first.L,second.L) && floatEquals(first.A,second.A) && floatEquals(first.B, second.B)
}

// D50 white reference [0.9642, 1.0000, 0.8251]
var testtable2 = []struct {
	c1   Lab
	c2   XYZ
	whiteref XYZ
}{
	{Lab{0, 0, 0}, XYZ{0, 0, 0}, XYZ{0.9642, 1.0000, 0.8251}},
	{Lab{93, -1, 25}, XYZ{0.794887, 0.829670, 0.446155}, XYZ{0.9642, 1.0000, 0.8251}},
	{Lab{46, -5, -29.5}, XYZ{0.139114, 0.152687, 0.261749}, XYZ{0.9642, 1.0000, 0.8251}},
	{Lab{70, -41.5, -25}, XYZ{0.275172, 0.407494, 0.536647}, XYZ{0.9642, 1.0000, 0.8251}},

}

func TestConvertToLab(t *testing.T) {
	for _, testcolour := range testtable2 {
		converted := testcolour.c2.convertToLab(testcolour.whiteref)
		//fmt.Printf("XYZ was correct, got: %+v, want: %+v.\n", converted, testcolour.c2)
		if !checkLabequal(converted, testcolour.c1) {
			t.Errorf("Lab was incorrect, got: %f, want: %f.", converted, testcolour.c1)
		}
	}


}