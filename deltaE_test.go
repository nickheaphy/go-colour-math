package colourmath

import (
	"math"
	"testing"
)

// Collection of colours and dE's to use for testing
// calculated using http://www.brucelindbloom.com/index.html?ColorDifferenceCalc.html
var coltables = []struct {
	c1   Lab
	c2   Lab
	de76 float64
	de94 float64
	de2k float64
}{
	{Lab{0, 0, 0}, Lab{0, 0, 0}, 0.0, 0.0, 0.0},
	{Lab{100, 0, 0}, Lab{0, 100, 0}, 141.42135, 141.42135, 104.6320},
	{Lab{93, -1, 25}, Lab{46, -5, -29.5}, 72.07808, 61.48521, 52.9995},
	{Lab{46, -5, -29.5}, Lab{93, -1, 25}, 72.07808, 60.20453, 52.99950},
	{Lab{70, -41.5, -25}, Lab{71, -40.5, -24.5}, 1.5, 1.060877, 0.847676},
	{Lab{100, -128, 128}, Lab{0, 128, -128}, 375.5955, 139.6268, 161.9958},
	{Lab{100, -128, 0}, Lab{0, 50, 0}, 204.1665, 114.61053, 133.91447},
}

// dirty way to check if floats are 'equal'
func floatEquals(a, b float64) bool {
	const eps float64 = 0.0001
	if math.Abs(a-b) < eps {
		return true
	}
	return false
}

func TestDE76(t *testing.T) {
	for _, table := range coltables {
		diff76 := DE76(table.c1, table.c2)
		if !floatEquals(diff76, table.de76) {
			t.Errorf("dE76 was incorrect, got: %f, want: %f.", diff76, table.de76)
		}
	}
}

func TestDE2000(t *testing.T) {
	for _, table := range coltables {
		diff2k := DE2000(table.c1, table.c2)
		if !floatEquals(diff2k, table.de2k) {
			t.Errorf("dE2k was incorrect, got: %f, want: %f.", diff2k, table.de2k)
		}
	}
}

func TestDE94(t *testing.T) {
	for _, table := range coltables {
		diff94 := DE94(table.c1, table.c2)
		if !floatEquals(diff94, table.de94) {
			t.Errorf("dE94 was incorrect, got: %f, want: %f.", diff94, table.de94)
		}
	}
}
