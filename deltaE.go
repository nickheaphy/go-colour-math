package colourmath

import (
	"math"
)

// Some unit conversion formulas
const (
	RadToDeg = 180 / math.Pi
	DegToRad = math.Pi / 180
)

// DE76 - Calculate the deltaE76 between reference (r) and sample (s) Lab values
func DE76(r1 Lab, s2 Lab) float64 {

	return math.Sqrt(math.Pow(r1.L-s2.L, 2) +
		math.Pow(r1.A-s2.A, 2) +
		math.Pow(r1.B-s2.B, 2))
}

// DE94 - Calculate dE94 for Graphic Arts between reference (r1) and sample (s2)
func DE94(r1 Lab, s2 Lab) float64 {
	return dE94full(r1, s2, 1, 0.045, 0.015)
}

// DE94T - Calculate dE94 for Textiles between reference (r1) and sample (s2)
func DE94T(r1 Lab, s2 Lab) float64 {
	return dE94full(r1, s2, 2, 0.048, 0.014)
}

// Calculate the deltaE94 between reference (r1) and sample (s2) Lab values
func dE94full(r1 Lab, s2 Lab, kl int, k1 float64, k2 float64) float64 {
	// http://www.Brucelindbloom.com/index.html?Eqn_DeltaE_CIE94.html
	dl := r1.L - s2.L
	// Could use math.Pow but this more readable IMHO
	c1 := math.Sqrt(r1.A*r1.A + r1.B*r1.B)
	c2 := math.Sqrt(s2.A*s2.A + s2.B*s2.B)
	dc := c1 - c2
	da := r1.A - s2.A
	db := r1.B - s2.B
	dh := math.Sqrt(math.Abs(da*da + db*db - dc*dc))
	const kc = 1
	const kh = 1
	const sl = 1
	sc := 1 + k1*c1
	sh := 1 + k2*c1
	return math.Sqrt(
		math.Pow(dl/(float64(kl)*sl), 2) +
			math.Pow(dc/(kc*sc), 2) +
			math.Pow(dh/(kh*sh), 2))
}

// DE2000 - Calculate the deltaE200 between reference (r1) and sample (s2) Lab values
func DE2000(r1 Lab, s2 Lab) float64 {
	// http://www.Brucelindbloom.com/index.html?Eqn_DeltaE_CIE2000.html
	// # Helped: https://github.com/THEjoezack/ColorMine/blob/master/ColorMine/ColorSpaces/Comparisons/CieDe2000Comparison.cs
	Lp := (r1.L + s2.L) / 2
	C1 := math.Sqrt(r1.A*r1.A + r1.B*r1.B)
	C2 := math.Sqrt(s2.A*s2.A + s2.B*s2.B)
	C := (C1 + C2) / 2
	G := 0.5 * (1 - math.Sqrt(math.Pow(C, 7)/(math.Pow(C, 7)+math.Pow(25, 7))))
	a1p := r1.A * (1 + G)
	a2p := s2.A * (1 + G)
	C1p := math.Sqrt(a1p*a1p + r1.B*r1.B)
	C2p := math.Sqrt(a2p*a2p + s2.B*s2.B)
	Cp := (C1p + C2p) / 2
	h1p := RadToDeg * (math.Atan2(r1.B, a1p))
	if h1p < 0 {
		h1p += 360
	}
	h2p := RadToDeg * (math.Atan2(s2.B, a2p))
	if h2p < 0 {
		h2p += 360
	}
	var Hp float64
	if math.Abs(h1p-h2p) > 180 {
		Hp = (h1p + h2p + 360) / 2
	} else {
		Hp = (h1p + h2p) / 2
	}
	T := (1 - 0.17*math.Cos(DegToRad*(Hp-30)) +
		0.24*math.Cos(DegToRad*(2*Hp)) +
		0.32*math.Cos(DegToRad*(3*Hp+6)) -
		0.20*math.Cos(DegToRad*(4*Hp-63)))
	var dhp float64
	if math.Abs(h2p-h1p) <= 180 {
		dhp = h2p - h1p
	} else if math.Abs(h2p-h1p) > 180 && h2p <= h1p {
		dhp = h2p - h1p + 360
	} else {
		dhp = h2p - h1p - 360
	}
	dLp := s2.L - r1.L
	dCp := C2p - C1p
	dHp := 2 * math.Sqrt(C1p*C2p) * math.Sin(DegToRad*(dhp/2))
	SL := 1 + ((0.015 * (Lp - 50) * (Lp - 50)) / math.Sqrt(20+(Lp-50)*(Lp-50)))
	SC := 1 + 0.045*Cp
	SH := 1 + 0.015*Cp*T
	dTheta := 30 * math.Exp(-1*math.Pow((Hp-275)/25, 2))
	RC := 2 * math.Sqrt(math.Pow(Cp, 7)/(math.Pow(Cp, 7)+math.Pow(25, 7)))
	RT := (-1 * RC) * math.Sin(DegToRad*(2*dTheta))
	const KL = 1
	const KC = 1
	const KH = 1
	return math.Sqrt(
		(dLp/(KL*SL))*(dLp/(KL*SL)) +
			(dCp/(KC*SC))*(dCp/(KC*SC)) +
			(dHp/(KH*SH))*(dHp/(KH*SH)) +
			RT*
				(dCp/(KC*SC))*(dHp/(KH*SH)))
}
