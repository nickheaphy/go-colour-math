# Colour Math Package

Experiments with Go and the math of colour and colourspaces.

Currently implements the following:

* DeltaE calculations (dE76, dE94 and dE2000) (still to implement dECMC)

* Structures to hold Lab and XYZ colours and conversion between the two.

A work in progress.